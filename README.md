# node-xmpp-vcard

A REST API to fetch XMPP vCards using a NodeJS backend.

Based on the repository by skorotkiewicz [0].

[0] https://github.com/skorotkiewicz/node-xmpp-vcard

## Install

### Preparation

- Make a dedicated account on a XMPP server (not all work, https://404.city is known to work);
- Update the environment variables using the credentials of the account during the installation process below.

### Install directly

```
git clone https://codeberg.org/keyoxide/node-xmpp-vcard
mv template.env .env
nano .env
npm install
npm start
```

## Install using docker

```
git clone https://codeberg.org/keyoxide/node-xmpp-vcard
mv template.env .env
nano .env
docker build -t node-xmpp-vcard .
docker run -p 3000:3000 -e NODE_ENV=production -d node-xmpp-vcard
```

## Install using docker-compose

```
git clone https://codeberg.org/keyoxide/node-xmpp-vcard
nano docker-compose.yml
docker-compose up -d
```

## Usage

```
/api/vcard/:jid/:data
```

## Data

| :data      | description  |
| ---------- | ------------ |
| `FN`       | Full name    |
| `NUMBER`   | Phone number |
| `USERID`   | Mail         |
| `URL`      | Website      |
| `BDAY`     | Birthday     |
| `NICKNAME` | Nickname     |
| `DESC`     | Description  |
