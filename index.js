/*
Copyright (C) 2020 Yarmo Mackenbach

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer network,
you should also make sure that it provides a way for users to get its source.
For example, if your program is a web application, its interface could display
a "Source" link that leads users to an archive of the code. There are many
ways you could offer source, and different solutions will be better for different
programs; see section 13 for the specific requirements.

You should also get your employer (if you work as a programmer) or school,
if any, to sign a "copyright disclaimer" for the program, if necessary. For
more information on this, and how to apply and follow the GNU AGPL, see <https://www.gnu.org/licenses/>.
*/
const express  = require('express');
const app      = express();
const cors     = require('cors');
const jsdom    = require("jsdom");
const {client, xml} = require('@xmpp/client');
const debug = require('@xmpp/debug');
require('dotenv').config()

const xmpp = client({
  service: process.env.service,
  username: process.env.username,
  password: process.env.password,
});

if (process.env.NODE_ENV !== 'production')
  debug(xmpp, true)

const {iqCaller} = xmpp
xmpp.start().catch(console.error)

app.use(cors());
app.get('/', info)
app.get('/api/vcard/:jid/:data', getVCard)

async function getVCard(req, res) {
  let jid = req.params.jid;
  let data = req.params.data.toUpperCase();

  let allowedData = ['FN', 'NUMBER', 'USERID', 'URL', 'BDAY', 'NICKNAME', 'DESC'];

  if (!allowedData.includes(data)) {
    return res.status(400).json('Allowed data are: FN, NUMBER, USERID, URL, BDAY, NICKNAME, DESC');
  }

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(jid)) {
    const response = await iqCaller.request(
      xml("iq", { type: "get", to: jid }, xml("vCard", "vcard-temp")), 30 * 1000
    );

    const vcardRow = response.getChild("vCard", "vcard-temp").toString();

    const dom = new jsdom.JSDOM(vcardRow);
    var vcard = dom.window.document.querySelector(data).textContent;

    return res.status(200).json(vcard);
  } else {
    return res.status(400).json('This is no valid JID.');
  }
}

function info(req, res) {
  return res.status(200).json('Usage: /api/vcard/:jid/:data -> Allowed data are: FN, NUMBER, USERID, URL, BDAY, NICKNAME, DESC');
}

// handle errors
if (process.env.NODE_ENV == 'production') {
  // eslint-disable-next-line no-unused-vars
  app.use(function(err, req, res, next) {
    if (err.status === 500)
      res.status(500).json({ message: "Something looks wrong" });
    else
      res.status(500).json({ message: "Something looks wrong" });
  });
}

// throw 404 if URL not found
app.all("*", function(req, res) {
  return res.status(404).json({ message: "Page not found" });
});

app.listen(3000, function() {
  console.log('Node server listening on port 3000');
});
